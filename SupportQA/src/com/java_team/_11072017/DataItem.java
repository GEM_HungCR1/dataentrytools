package com.java_team._11072017;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GEM on 2017-07-11.
 */
public class DataItem
{
    public String label;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String gender;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String age;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String emotion;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Double> x_y_w_h = new ArrayList<Double>();


}
