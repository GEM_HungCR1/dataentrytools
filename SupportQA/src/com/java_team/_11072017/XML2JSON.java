//package com.java_team._11072017;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.dataformat.xml.XmlMapper;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.json.XML;
//
//import java.io.*;
//
//
///**
// * Created by GEM on 2017-07-11.
// */
//public class XML2JSON
//{
//    public static String ROOT = "F:\\Support-QA\\20170713\\Folder ";
//
//    public static void main(String[] args)
//    {
//        for (int folderIndex = 1; folderIndex < 9; folderIndex++)
//        {
//            String FOLDER = ROOT + folderIndex + "\\";
//
//            System.out.println("READ FOLDER: " + FOLDER);
//
//            ObjectMapper jsonMapper = new ObjectMapper();
//
//            File file = new File(FOLDER);
//            File output = new File(file, "annotations");
//            if (!output.exists())
//            {
//                output.mkdirs();
//            }
//
//            int count = 0;
//            for (File f : file.listFiles())
//            {
//
//                PrintWriter out = null;
//                try
//                {
//                    if (f.isDirectory())
//                    {
//                        continue;
//                    }
//
//                    if (f.getName().startsWith("."))
//                    {
//                        continue;
//                    }
//
//                    if (!f.getName().endsWith(".xml"))
//                    {
//                        continue;
//                    }
//
//                    JSONObject xmlJSONObj = XML.toJSONObject(readFile(f));
//
//                    if (!xmlJSONObj.isNull("annotation"))
//                    {
//                        JSONObject jsonObject = xmlJSONObj.getJSONObject("annotation");
//
//                        DataModel model = new DataModel();
//                        model.image_path = jsonObject.getString("filename");
//
//                        model.image_w_h.add(jsonObject.getJSONObject("size").getDouble("width"));
//                        model.image_w_h.add(jsonObject.getJSONObject("size").getDouble("height"));
//
//                        if (!jsonObject.isNull("object"))
//                        {
//                            Object intervention = jsonObject.get("object");
//                            if (intervention instanceof JSONArray)
//                            {
//                                JSONArray jsonObjects = jsonObject.getJSONArray("object");
//                                for (int i = 0; i < jsonObjects.length(); i++)
//                                {
//                                    DataItem item = new DataItem();
//
//                                    JSONObject object = jsonObjects.getJSONObject(i);
//                                    item.label = object.getString("name");
//
//                                    Double xmin = object.getJSONObject("bndbox").getDouble("xmin");
//                                    Double ymin = object.getJSONObject("bndbox").getDouble("ymin");
//                                    Double xmax = object.getJSONObject("bndbox").getDouble("xmax");
//                                    Double ymax = object.getJSONObject("bndbox").getDouble("ymax");
//                                    item.x_y_w_h.add(xmin);
//                                    item.x_y_w_h.add(ymin);
//                                    item.x_y_w_h.add(xmax - xmin);
//                                    item.x_y_w_h.add(ymax - ymin);
//                                    count++;
//                                    model.objects.add(item);
//
//
//                                }
//
//                            }
//
//                            if (intervention instanceof JSONObject)
//                            {
//                                JSONObject object = jsonObject.getJSONObject("object");
//                                DataItem item = new DataItem();
//                                item.label = object.getString("name");
//                                Double xmin = object.getJSONObject("bndbox").getDouble("xmin");
//                                Double ymin = object.getJSONObject("bndbox").getDouble("ymin");
//                                Double xmax = object.getJSONObject("bndbox").getDouble("xmax");
//                                Double ymax = object.getJSONObject("bndbox").getDouble("ymax");
//                                item.x_y_w_h.add(xmin);
//                                item.x_y_w_h.add(ymin);
//                                item.x_y_w_h.add(xmax - xmin);
//                                item.x_y_w_h.add(ymax - ymin);
//
//                                count++;
//                                model.objects.add(item);
//                            }
//
//                        }
//
//                        String absolutePath = "";
//                        if (f.getAbsolutePath().endsWith(".xml"))
//                        {
//                            absolutePath = output.getAbsoluteFile() + "/" + f.getName().replace(".xml", ".json");
//                        }
//                        out = new PrintWriter(new File(absolutePath));
//
//                        JSONObject newObject = new JSONObject(jsonMapper.writeValueAsString(model));
//                        out.println(newObject.toString(4));
//                    }
//
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//                finally
//                {
//                    if (out != null)
//                    {
//                        out.close();
//                    }
//                }
//
//                PrintWriter outPrintCount = null;
//                try
//                {
//                    outPrintCount = new PrintWriter(new File(output, "count.txt"));
//                    outPrintCount.print(count);
//                }
//                catch (Exception e)
//                {
//
//                }
//                finally
//                {
//                    if (outPrintCount != null)
//                    {
//                        outPrintCount.close();
//                    }
//                }
//            }
//        }
//    }
//
//    private static String readFile(File f)
//    {
//        BufferedReader br = null;
//        FileReader fr = null;
//
//        try {
//
//            fr = new FileReader(f);
//            br = new BufferedReader(fr);
//
//            String sCurrentLine;
//
//            StringBuilder sb = new StringBuilder();
//            while ((sCurrentLine = br.readLine()) != null)
//            {
//                sb.append(sCurrentLine);
//            }
//
//            return sb.toString();
//        } catch (IOException e) {
//
//            e.printStackTrace();
//
//        } finally {
//
//            try {
//
//                if (br != null)
//                    br.close();
//
//                if (fr != null)
//                    fr.close();
//
//            } catch (IOException ex) {
//
//                ex.printStackTrace();
//
//            }
//
//        }
//        return "";
//    }
//}
