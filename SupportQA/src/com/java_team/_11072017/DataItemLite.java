package com.java_team._11072017;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GEM on 2017-07-11.
 */
public class DataItemLite
{
    public String label;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String gender;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String age;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String emotion;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String person;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Double> Head = new ArrayList<Double>();
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Double> Upper_body = new ArrayList<Double>();
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Double> Lower_body = new ArrayList<Double>();
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public List<Double> entire_body = new ArrayList<Double>();


}
