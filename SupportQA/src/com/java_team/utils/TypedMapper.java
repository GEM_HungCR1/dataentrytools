package com.java_team.utils;

/**
 * Created by GEM on 2017-07-17.
 */
public interface TypedMapper<S1, S2>
{
    public S2 invokeMapperAction(S1 ik);

}
