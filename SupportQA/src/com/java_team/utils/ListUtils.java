package com.java_team.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GEM on 2017-07-17.
 */
public class ListUtils
{
    public static<T> Map<String, List<T>> group(List<T> items, TypedMapper<T, String> lf)
    {
        Map<String, List<T>> res = new LinkedHashMap<>();

        for (T ik: items)
        {
            String vk = lf.invokeMapperAction(ik);
            List<T> gk = res.get(vk);

            if (gk == null)
            {
                res.put(vk, gk = new ArrayList<>());
            }
            gk.add(ik);
        }

        return res;
    }

    public static<T1,T2> Map<T2, Double> hist(List<T1> items, TypedMapper<T1, T2> lf)
    {
        Map<T2, Double> res = new LinkedHashMap<>();

        for (T1 ik: items)
        {
            T2 vk = lf.invokeMapperAction(ik);
            Double ck = res.get(vk);
            res.put(vk, ck == null ? 1 : ck + 1);
        }

        return res;
    }
}
