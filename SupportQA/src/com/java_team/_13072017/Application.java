package com.java_team._13072017;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.java_team._11072017.DataItem;
import com.java_team._11072017.DataItemLite;
import com.java_team._11072017.DataModel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

/**
 * Created by GEM on 2017-07-14.
 */
public class Application extends JFrame
{

    public static void main(String[] args)
    {

        EventQueue.invokeLater(() ->
        {
            JFrame frame = new JFrame();
            frame.setSize(450, 100);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            Container container = frame.getContentPane();
            container.setLayout(new FlowLayout());

            JTextField textField = new JTextField();
            textField.setPreferredSize(new Dimension(300, 25));

            JLabel label = new JLabel("Thêm folder gốc vào đây");

            JButton okButton = new JButton("Chạy thôi :))");
            okButton.addActionListener((e) ->
            {

                String input = textField.getText();
                convertAction(input);
                label.setText("Success :D");

            });

            container.add(textField);
            container.add(okButton);
            container.add(label);

            frame.setVisible(true);
        });
    }

    public static void convertAction(String root)
    {
        File stock = new File(root);
        for (File folder : stock.listFiles())
        {
            if (!folder.isDirectory())
            {
                continue;
            }
            String FOLDER = folder.getAbsolutePath();

            ObjectMapper jsonMapper = new ObjectMapper();

            File file = new File(FOLDER);
            File output = new File(file, "annotations");
            if (!output.exists())
            {
                output.mkdirs();
            }

            int count = 0;
            for (File f : file.listFiles())
            {

                PrintWriter out = null;
                try
                {
                    if (f.isDirectory())
                    {
                        continue;
                    }

                    if (f.getName().startsWith("."))
                    {
                        continue;
                    }

                    if (!f.getName().endsWith(".xml"))
                    {
                        continue;
                    }

                    JSONObject xmlJSONObj = XML.toJSONObject(readFile(f));

                    if (!xmlJSONObj.isNull("annotation"))
                    {
                        JSONObject jsonObject = xmlJSONObj.getJSONObject("annotation");

                        DataModel model = new DataModel();
                        model.image_path = jsonObject.getString("filename");

                        model.image_w_h.add(jsonObject.getJSONObject("size").getDouble("width"));
                        model.image_w_h.add(jsonObject.getJSONObject("size").getDouble("height"));

                        if (!jsonObject.isNull("object"))
                        {
                            Object intervention = jsonObject.get("object");
                            if (intervention instanceof JSONArray)
                            {
                                JSONArray jsonObjects = jsonObject.getJSONArray("object");
                                for (int i = 0; i < jsonObjects.length(); i++)
                                {
//                                    DataItem item = new DataItem();
//                                    DataItemLite dataitem = new DataItemLite();
                                    JSONObject object = jsonObjects.getJSONObject(i);
                                    String label = object.getString("name");
//                                    item.label = label;
                                    String[] ck = label.split(",");
//                                    if (ck.length == 2)
//                                    {
//                                        item.gender = ck[1].trim();
//                                    }
//                                    else if (ck.length == 3)
//                                    {
//                                        item.gender = ck[1].trim();
//                                        item.age = ck[2].trim();
//                                    }
//                                    else if (ck.length == 4)
//                                    {
//                                        item.gender = ck[1].trim();
//                                        item.age = ck[2].trim();
//                                        item.emotion = ck[3].trim();
//                                    }
//                                    else if (ck.length >= 5)
//                                    {
//                                        item.gender = ck[2].trim();
//                                        item.age = ck[3].trim();
//                                        item.emotion = ck[4].trim();
//                                    }
                                    Double xmin = object.getJSONObject("bndbox").getDouble("xmin");
                                    Double ymin = object.getJSONObject("bndbox").getDouble("ymin");
                                    Double xmax = object.getJSONObject("bndbox").getDouble("xmax");
                                    Double ymax = object.getJSONObject("bndbox").getDouble("ymax");
//                                    item.x_y_w_h.add(xmin);
//                                    item.x_y_w_h.add(ymin);
//                                    item.x_y_w_h.add(xmax - xmin);
//                                    item.x_y_w_h.add(ymax - ymin);
//                                    count++;
//                                    model.objects.add(item);


                                    //1. Get ID : 2 cases
                                    String[] nameArrays = label.split(" ");
                                    String id = nameArrays[0];
                                    boolean isMain = false;
                                    if (id.contains(","))
                                    {
                                        id = id.replace(",", "");
                                        isMain = true;
                                    }


                                    //2. Find DataItemLite By ID in model.objectLites
                                    DataItemLite dataItemLite = findById(id, model.objects);

                                    boolean isNew = false;
                                    if (dataItemLite == null)
                                    {
                                        isNew = true;
                                        dataItemLite = new DataItemLite();
                                        dataItemLite.person = id;
                                    }


                                    if (isMain)
                                    {
                                        dataItemLite.label = label;
                                        if (ck.length == 2)
                                        {
                                            dataItemLite.gender = ck[1].trim();
                                        }
                                        else if (ck.length == 3)
                                        {
                                            dataItemLite.gender = ck[1].trim();
                                            dataItemLite.age = ck[2].trim();
                                        }
                                        else if (ck.length == 4)
                                        {
                                            dataItemLite.gender = ck[1].trim();
                                            dataItemLite.age = ck[2].trim();
                                            dataItemLite.emotion = ck[3].trim();
                                        }
                                        else if (ck.length >= 5)
                                        {
                                            dataItemLite.gender = ck[2].trim();
                                            dataItemLite.age = ck[3].trim();
                                            dataItemLite.emotion = ck[4].trim();
                                        }

                                        dataItemLite.entire_body.add(xmin);
                                        dataItemLite.entire_body.add(ymin);
                                        dataItemLite.entire_body.add(xmax - xmin);
                                        dataItemLite.entire_body.add(ymax - ymin);
                                    }
                                    else
                                    {
                                        String type = nameArrays[1];
                                        switch (type)
                                        {
                                            case "Head":
                                                dataItemLite.Head.add(xmin);
                                                dataItemLite.Head.add(ymin);
                                                dataItemLite.Head.add(xmax - xmin);
                                                dataItemLite.Head.add(ymax - ymin);
                                                break;
                                            case "Lower":
                                                dataItemLite.Lower_body.add(xmin);
                                                dataItemLite.Lower_body.add(ymin);
                                                dataItemLite.Lower_body.add(xmax - xmin);
                                                dataItemLite.Lower_body.add(ymax - ymin);
                                                break;
                                            case "Upper":
                                                dataItemLite.Upper_body.add(xmin);
                                                dataItemLite.Upper_body.add(ymin);
                                                dataItemLite.Upper_body.add(xmax - xmin);
                                                dataItemLite.Upper_body.add(ymax - ymin);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    if (isNew)
                                    {
                                        model.objects.add(dataItemLite);
                                    }


                                }

                            }

//                            if (intervention instanceof JSONObject)
//                            {
//                                JSONObject object = jsonObject.getJSONObject("object");
//                                DataItem item = new DataItem();
//                                item.label = object.getString("name");
//                                Double xmin = object.getJSONObject("bndbox").getDouble("xmin");
//                                Double ymin = object.getJSONObject("bndbox").getDouble("ymin");
//                                Double xmax = object.getJSONObject("bndbox").getDouble("xmax");
//                                Double ymax = object.getJSONObject("bndbox").getDouble("ymax");
//                                item.x_y_w_h.add(xmin);
//                                item.x_y_w_h.add(ymin);
//                                item.x_y_w_h.add(xmax - xmin);
//                                item.x_y_w_h.add(ymax - ymin);
//                                count++;
//                                model.objects.add(item);
//                            }

                        }

                        for (DataItemLite dataItemLite : model.objects)
                        {
                            dataItemLite.person = null;
                        }

                        String absolutePath = "";
                        if (f.getAbsolutePath().endsWith(".xml"))
                        {
                            absolutePath = output.getAbsoluteFile() + "/" + f.getName().replace(".xml", ".json");
                        }
                        out = new PrintWriter(new File(absolutePath));

                        JSONObject newObject = new JSONObject(jsonMapper.writeValueAsString(model));
                        out.println(newObject.toString(4));
                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    if (out != null)
                    {
                        out.close();
                    }
                }

                PrintWriter outPrintCount = null;
                try
                {
                    outPrintCount = new PrintWriter(new File(output, "count.txt"));
                    outPrintCount.print(count);
                }
                catch (Exception e)
                {

                }
                finally
                {
                    if (outPrintCount != null)
                    {
                        outPrintCount.close();
                    }
                }
            }
        }
    }

    private static DataItemLite findById(String id, List<DataItemLite> objectLites)
    {
        for (DataItemLite dataItemLite : objectLites)
        {
            if (id.equals(dataItemLite.person))
            {
                return dataItemLite;
            }
        }
        return null;
    }

    private static String readFile(File f)
    {
        BufferedReader br = null;
        FileReader fr = null;

        try
        {

            fr = new FileReader(f);
            br = new BufferedReader(fr);

            String sCurrentLine;

            StringBuilder sb = new StringBuilder();
            while ((sCurrentLine = br.readLine()) != null)
            {
                sb.append(sCurrentLine);
            }

            return sb.toString();
        }
        catch (IOException e)
        {

            e.printStackTrace();

        }
        finally
        {

            try
            {

                if (br != null)
                {
                    br.close();
                }

                if (fr != null)
                {
                    fr.close();
                }

            }
            catch (IOException ex)
            {

                ex.printStackTrace();

            }

        }
        return "";
    }
}
