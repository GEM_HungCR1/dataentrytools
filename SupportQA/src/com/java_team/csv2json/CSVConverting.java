//package com.java_team.csv2json;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.java_team._11072017.DataModel;
//import com.java_team.ROOT;
//import com.java_team._11072017.DataItem;
//import com.java_team.utils.ListUtils;
//import com.java_team.utils.TomcatFileManager;
//import org.apache.commons.io.FileUtils;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.io.IOException;
//
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
///**
// * Created by GEM on 2017-07-17.
// */
//public class CSVConverting
//{
//    private static TomcatFileManager fileManager = new TomcatFileManager();
//    private static ObjectMapper mapper = new ObjectMapper();
//
//    public static void main(String[] args)
//    {
//        File output = fileManager.getDesktopFile("output" + System.currentTimeMillis());
//        if (!output.exists())
//        {
//            output.mkdirs();
//        }
//
//        String stock = System.getProperty("user.dir");
//        try
//        {
//            List<String> rows = FileUtils.readLines(new File(stock, "/resource/trendi1.csv"));
//            Map<String, List<ImageObject>> groupList = ListUtils.group(convertToModel(rows), x -> x.imageId);
//
//            for (String sk: groupList.keySet())
//            {
//                List<ImageObject> imageObjectList = groupList.get(sk);
//                if (imageObjectList.size() > 0)
//                {
//                    String fileName = imageObjectList.get(0).timestamp
//                            .replaceAll(":", "_")
//                            .replaceAll(" ", "_")
//                            .replaceAll("-", "_");
//
//                    fileName += "_" + sk + ".json";
//
//                    DataModel dataModel = new DataModel();
//                    dataModel.image_path = imageObjectList.get(0).path;
//
//                    for (ImageObject image: imageObjectList)
//                    {
//                        DataItem dataItem = new DataItem();
//                        dataItem.label = image.description;
//                        dataItem.x_y_w_h.add((double)image.boundingBoxX);
//                        dataItem.x_y_w_h.add((double)image.boundingBoxY);
//                        dataItem.x_y_w_h.add((double)image.boundingBoxWidth);
//                        dataItem.x_y_w_h.add((double)image.boundingBoxHight);
//
//                        dataModel.objects.add(dataItem);
//                    }
//
//                    PrintWriter out = null;
//                    try
//                    {
//                        out = new PrintWriter(new File(output, fileName));
//                        JSONObject newObject = new JSONObject(mapper.writeValueAsString(dataModel));
//                        out.println(newObject.toString(4));
//                    }
//                    catch (Exception e)
//                    {
//                        e.printStackTrace();
//                    }
//                    finally
//                    {
//                        if (out != null)
//                        {
//                            out.close();
//                        }
//                    }
//
//
//                }
//            }
//
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//
//    }
//
//    private static List<ImageObject> convertToModel(List<String> rows)
//    {
//        List<ImageObject> res = new ArrayList<>();
//
//        for (String row: rows)
//        {
//            String[] cell = row.replaceAll("\"", "").split(",");
//
//            ImageObject image = new ImageObject();
//            image.imageId = cell[1];
//            image.boundingBoxX = Integer.parseInt(cell[5]);
//            image.boundingBoxY = Integer.parseInt(cell[6]);
//            image.boundingBoxWidth = Integer.parseInt(cell[7]);
//            image.boundingBoxHight = Integer.parseInt(cell[8]);
//            image.description = cell[12];
//            image.timestamp = cell[16];
//            image.path = cell[17];
//
//            res.add(image);
//        }
//
//        return res;
//    }
//}
