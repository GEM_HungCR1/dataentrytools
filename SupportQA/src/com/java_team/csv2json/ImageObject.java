package com.java_team.csv2json;

/**
 * Created by GEM on 2017-07-17.
 */
public class ImageObject
{
    public String imageId;
    public int boundingBoxX;
    public int boundingBoxY;
    public int boundingBoxWidth;
    public int boundingBoxHight;
    public String description;
    public String timestamp;
    public String path;

}
