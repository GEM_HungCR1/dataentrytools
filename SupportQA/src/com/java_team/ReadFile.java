package com.java_team;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.Scanner;

/**
 * Created by GEM on 2017-07-06.
 */
public class ReadFile
{
    private static final String FOLDER = "F:\\Support-QA\\annotations";

    public static void main(String[] args)
    {
        /*Scanner scanner = new Scanner(System.in);
        System.out.print("Enter input folder: ");
        String inputFolder = scanner.nextLine();
        System.out.print("Enter output folder: ");
        String outputFolder = scanner.nextLine();*/

        File output = new File(FOLDER + "\\output");
        if (!output.exists()) output.mkdirs();

        ObjectMapper mapper = new ObjectMapper();
        File file = new File(FOLDER);
        for (File f: file.listFiles())
        {
            if (f == null) continue;
            System.out.println("Reading file: " + f.getName());
            try
            {
                DataModel model = mapper.readValue(f, DataModel.class);

                String[] s = model.image_path.split("/");
                model.image_path = s[s.length - 1];

                mapper.writeValue(new File(output, f.getName()), model);

            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

}
