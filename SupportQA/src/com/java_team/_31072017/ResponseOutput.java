package com.java_team._31072017;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GEM on 2017-07-31.
 */
public class ResponseOutput
{
    public String image_path;
    public String image_size;
    public List<Tags> tags = new ArrayList<>();
}
