//package com.java_team._31072017;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.java_team._11072017.DataItem;
//import com.java_team._11072017.DataModel;
//import org.json.JSONObject;
//
//import java.io.File;
//import java.io.PrintWriter;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
///**
// * Created by GEM on 2017-07-31.
// */
//public class converting_data
//{
//    private static File STOCK = new File("F:\\Support-QA\\20170731\\annotations");
//    private static ObjectMapper mapper = new ObjectMapper();
//
//    public static void main(String[] args)
//    {
//        File output = new File(STOCK, "output");
//        if (!output.exists())
//        {
//            output.mkdirs();
//        }
//
//
//
//        for (File fk: STOCK.listFiles())
//        {
//            if (fk.isDirectory()) continue;
//            if (fk.getName().matches(".*([.])txt"))
//            {
//                continue;
//            }
//
//
//            try
//            {
//                ResponseOutput response = new ResponseOutput();
//
//                DataModel dataModel = mapper.readValue(fk, DataModel.class);
//                response.image_path = dataModel.image_path;
//                response.image_size = dataModel.image_w_h.get(0) + "x" + dataModel.image_w_h.get(1);
//
//                for (DataItem dataItem: dataModel.objects)
//                {
//                    Tags tag = new Tags();
//
//                    Map<String, Integer> mapLocation = new LinkedHashMap<>();
//                    mapLocation.put("top", dataItem.x_y_w_h.get(0).intValue());
//                    mapLocation.put("left", dataItem.x_y_w_h.get(1).intValue());
//                    mapLocation.put("width", dataItem.x_y_w_h.get(2).intValue());
//                    mapLocation.put("height", dataItem.x_y_w_h.get(3).intValue());
//                    tag.tag_location = mapLocation;
//
//                    String[] tags = dataItem.label.split("-");
//                    tag.tag_category = "Furniture";
//                    for (String tk: tags)
//                    {
//                        tag.tag_category += "," + tk;
//                    }
//
//                    response.tags.add(tag);
//                }
//
//                //mapper.writeValue(new File(output, fk.getName()), response);
//
//                try (PrintWriter out = new PrintWriter(new File(output, fk.getName())))
//                {
//                    JSONObject newObject = new JSONObject(mapper.writeValueAsString(response));
//                    out.println(newObject.toString(4));
//                }
//                catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//            }
//            catch (Exception e)
//            {
//                e.printStackTrace();
//                System.out.println("----------- Exception: " + fk.getName());
//            }
//
//
//
//        }
//
//    }
//}
