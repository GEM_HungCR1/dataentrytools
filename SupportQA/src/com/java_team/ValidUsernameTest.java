package com.java_team;

import java.util.regex.Pattern;

/**
 * Created by GEM on 2017-07-31.
 */
public class ValidUsernameTest
{
    public static final String REGEX = "^[a-zA-Z0-9_]*$";

    public static boolean isValidUsername(String username) {
        return username.matches(REGEX);
    }

    public static void main(String[] arsg)
    {
        System.out.println(isValidUsername("anhld"));
        System.out.println(isValidUsername("anhld58"));
        System.out.println(isValidUsername("Anhld58"));
        System.out.println(isValidUsername("Anhld_58"));
        System.out.println(isValidUsername("anhld5@gemvietnam.com"));
    }
}
